create table Book(book_id integer primary key auto_increment, 
            book_title varchar(500), 
            publisher_name varchar(500), 
            author_name varchar(500));

insert into Book( book_title, publisher_name, author_name) values('AAA','A','AA');
insert into Book( book_title, publisher_name, author_name) values('BBB','B','BB');